on *:TEXT:$addban *:*: {
  if ($Nick isop $chan) { 
    writeini bans.ini $chan $2 $2
    notice $nick $2 Is banned from $chan $+ .
    mode $chan +b $2
  }
}
on *:TEXT:$bans:#: {
  if ($Nick isop $chan) { 
    notice $nick  $+ $chan Bans.
    var %c 0
    var %t $ini(bans.ini, $chan, 0)
    while (%c < %t) {
      inc %c
      notice $nick $readini(bans.ini, $chan, $ini(bans.ini, $chan, %c))
    }
    notice $nick  $+ %c bans found.
  }
  else { notice $nick You lack access to $chan $+ .
  }
}
on *:TEXT:$delban *:*: {
  if ($Nick isop $chan) { 
    remini bans.ini $chan $2
    notice $nick $2 Is unbanned from $chan $+ .
    mode $chan -b $2
  }
}
on *:TEXT:$kb *:*: {
  if ($Nick isop $chan) {  
    writeini bans.ini $chan $address($2,2) $address($2,2)
    mode $chan +b $address($2,2)
    kick $2 $chan kickbanned by $nick
  }
}
on *:JOIN:#: {
  if ($readini(bans.ini, $chan, $address($nick, 2))) { 
    mode $chan +b $address($nick, 2)
    kick $nick $chan Banned.
  }
}